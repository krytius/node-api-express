## Node API Express 

### Pre requisitos

* Node.js 18+

## VARIABLES

```
ENVIRONMENT = Ambiente;
NAME = Nome da Aplicação;
NODE_PATH = NODE_PATH;
PORT = Porta em que o serviço vai rodar;
URL = Url em que o serviço vai responder;
FILE_SIZE_POST_MB = Tamanho máximo de um post;
LEVEL_LOG = Biblioteca Log debug;
LOG_DIR = Diretório de logs;
DEBUG_POST = Print dos requests;
REQUEST_TIME_OUT = Tempo para uma execução falhar
CORS = Habilitar cors
```
import {Log, Main} from "../src/index";

class App {

    constructor() {
        this.init();
    }

    init(): void {
        new Main();
    }
}

const app = new App();

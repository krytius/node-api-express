import {Response, IRequest, IResponse, IRouteController} from "../../src/index";

export class Home implements IRouteController {
    get(req: IRequest, res: IResponse) {
        Response.success(res, {message: "OK"});
    }
}
import {Response, IRequest, IResponse, IRouteController} from "../../../src/index";

export class Test implements IRouteController {
    get(req: IRequest, res: IResponse) {
        Response.success(res, {message: "OK"});
    }
}
module.exports = {
    preset: "ts-jest",
    testEnvironment: "node",
    clearMocks: true,
    coverageDirectory: "coverage",
    moduleFileExtensions: ["js", "json", "jsx", "ts", "tsx", "node"],
};
import {Routes} from "./Routes";
import {Log} from "../helpers/Log";

export class Main {

    private logger: Log;

    constructor() {
        this.logger = new Log(__filename);
        this.init()
            .then(() => {
                this.logger.success('Program initialized.');
            });
    }

    private async init() {
        this.logger.success('Program start.');
        let routes = new Routes();
        routes.init();
        return true;
    }
}

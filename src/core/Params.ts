export class Params {

    // CONFIG
    public static DIR_SRC = process.env.NODE_PATH ?? './';
    public static NAME = process.env.NAME ?? 'server';
    public static PORT = process.env.PORT ? process.env.PORT : 3000;
    public static URL = process.env.URL ? process.env.URL : "localhost";
    public static PROTOCOL = process.env.PROTOCOL ? process.env.PROTOCOL : "http://";
    public static FILE_SIZE_POST_MB = process.env.FILE_SIZE_POST_M ? process.env.FILE_SIZE_POST_M : 10;
    public static LEVEL_LOG = process.env.LEVEL_LOG ?? 'debug';
    public static LOG_DIR = process.env.LOG_DIR ?? process.cwd();
    public static DEBUG_POST = process.env.DEBUG_POST === "true";
    public static CORS = process.env.CORS === "true";

}
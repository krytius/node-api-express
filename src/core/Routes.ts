import {INextFunction, IRequest, IResponse, IRouteController} from "../interface/IRouteController";
import {Params} from "./Params";
import {Log} from "../helpers/Log";
import Express from "express";
import cors from 'cors';
import {json, urlencoded, raw} from "body-parser";
import expressEjsLayouts from "express-ejs-layouts";
import * as fs from "fs";
import * as path from "path";
import clc from "cli-color";

export class Routes {

    private logger: Log;

    constructor() {
        this.logger = new Log(__filename);
    }

    private config(app: any): any {
        if (Params.CORS) {
            app.use(cors());
        }
        app.set("view engine", "ejs");
        app.set("views", path.join(Params.DIR_SRC, "views"));
        app.use(expressEjsLayouts);
        app.use(json({
            limit: `${Params.FILE_SIZE_POST_MB}mb`
        }));
        app.use(urlencoded({
            limit: `${Params.FILE_SIZE_POST_MB}mb`,
            extended: true,
            parameterLimit: 100000
        }));
        app.use(raw({
            limit: `${Params.FILE_SIZE_POST_MB}mb`
        }));
        app.use(Express.static(path.join(Params.DIR_SRC, "public")));
        return app;
    }

    private getFiles() {
        let pathController = path.join(Params.DIR_SRC, `controllers`);
        let files = fs.readdirSync(pathController);

        this.logger.info(`Routes`);

        let filesPath: string[] = [];
        for (const file of files) {

            let fullPath = path.join(pathController, file);
            if (fs.lstatSync(fullPath).isDirectory()) {
                filesPath = this.mountRoute(fullPath, pathController, filesPath);
            } else {
                if (path.extname(file) !== ".ts" && path.extname(file) !== ".js") {
                    continue;
                }
                let name = file.replace(".ts", "");
                name = name.replace(".js", "");
                filesPath.push(path.join(`/`, name));
            }
        }
        return filesPath;
    }

    private mountRoute(dir: string, pathController: string, filesPath: string[]) {
        let filesArray = fs.readdirSync(dir);
        for (let key in filesArray) {
            let file = filesArray[key];
            let fullPath = path.join(dir, file);
            if (fs.lstatSync(fullPath).isDirectory()) {
                filesPath = this.mountRoute(fullPath, pathController, filesPath);
            } else {
                if (path.extname(file) !== ".ts" && path.extname(file) !== ".js") {
                    continue;
                }
                let name = file.replace(".ts", "");
                name = name.replace(".js", "");
                let pt = dir.replace(pathController, "");
                filesPath.push(`${pt}/${name}`);
            }
        }

        return filesPath;
    }

    public async init(): Promise<void> {
        try {
            let app = Express();
            app = this.config(app);

            let pathController = path.join(process.cwd(), Params.DIR_SRC, `controllers`);
            let filesPath: string[] = this.getFiles();

            let defaultMiddleware = (
                req: IRequest,
                res: IResponse,
                next: INextFunction
            ) => {
                next();
            };

            for (const file of filesPath) {
                let fileItem = file.replace(`${pathController}`, "");
                let RouteItem = require(`${pathController}${fileItem}`);
                let keys = Object.keys(RouteItem);
                let routeName = fileItem.toLowerCase().split("\\").join("/");
                let route: IRouteController = <IRouteController>(new RouteItem[keys[0]]());

                if (routeName === "/index") {
                    routeName = "/";
                }

                this.createGet(app, routeName, defaultMiddleware, route);
                this.createGetItem(app, routeName, defaultMiddleware, route);
                this.createPost(app, routeName, defaultMiddleware, route);
                this.creatPut(app, routeName, defaultMiddleware, route);
                this.createDelete(app, routeName, defaultMiddleware, route);
            }

            const p = new Promise((resolve) => {
                app.listen(Params.PORT, () => {
                    this.logger.success(`Server run ${Params.PROTOCOL}${Params.URL}:${Params.PORT}`);
                    resolve(true);
                });
            });
            await p;
        } catch (e) {
            this.logger.error(e);
        }
    }

    private createGet(app: any, routeName: string, defaultMiddleware: any, route: IRouteController) {
        if (route.get && typeof (route.get) === "function") {
            app.get(
                `${routeName}`,
                (route.middleware) ? route.middleware : defaultMiddleware,
                (req: IRequest, res: IResponse) => {
                    if (route.get) {
                        route.get(req, res);
                        return;
                    }
                    this.logger.error(`Route GET ${routeName} not found.`);
                }
            );
            const type: string = clc.green("GET".padEnd(10, " "));
            this.logger.info(`${type} ${Params.PROTOCOL}${Params.URL}:${Params.PORT}${routeName}`);
        }
    }

    private createGetItem(app: any, routeName: string, defaultMiddleware: any, route: IRouteController) {
        if (route.getItem && typeof (route.getItem) === "function") {
            app.get(
                `${routeName}/:id`,
                (route.middleware) ? route.middleware : defaultMiddleware,
                (req: IRequest, res: IResponse) => {
                    if (route.getItem) {
                        route.getItem(req, res);
                        return;
                    }
                    this.logger.error(`Route GET ${routeName}/:id not found.`);
                }
            );
            const type: string = clc.green("GET".padEnd(10, " "));
            this.logger.info(`${type} ${Params.PROTOCOL}${Params.URL}:${Params.PORT}${routeName}/:id`);
        }
    }

    private createPost(app: any, routeName: string, defaultMiddleware: any, route: IRouteController) {
        if (route.post && typeof (route.post) === "function") {
            app.post(
                `${routeName}`,
                (route.middleware) ? route.middleware : defaultMiddleware,
                (req: IRequest, res: IResponse) => {
                    if (route.post) {
                        route.post(req, res);
                        return;
                    }
                    this.logger.error(`Route POST ${routeName} not found.`);
                }
            );
            const type: string = clc.yellow("POST".padEnd(10, " "));
            this.logger.info(`${type} ${Params.PROTOCOL}${Params.URL}:${Params.PORT}${routeName}`);
        }
    }

    private creatPut(app: any, routeName: string, defaultMiddleware: any, route: IRouteController) {
        if (route.put && typeof (route.put) === "function") {
            app.put(
                `${routeName}/:id`,
                (route.middleware) ? route.middleware : defaultMiddleware,
                (req: IRequest, res: IResponse) => {
                    if (route.put) {
                        route.put(req, res);
                        return;
                    }
                    this.logger.error(`Route PUT ${routeName}/:id not found.`);
                }
            );
            const type: string = clc.blue("PUT".padEnd(10, " "));
            this.logger.info(`${type} ${Params.PROTOCOL}${Params.URL}:${Params.PORT}${routeName}/:id`);
        }
    }

    private createDelete(app: any, routeName: string, defaultMiddleware: any, route: IRouteController) {
        if (route.delete && typeof (route.delete) === "function") {
            app.delete(
                `${routeName}/:id`,
                (route.middleware) ? route.middleware : defaultMiddleware,
                (req: IRequest, res: IResponse) => {
                    if (route.delete) {
                        route.delete(req, res);
                        return;
                    }
                    this.logger.error(`Route DELETE ${routeName}/:id not found.`);
                }
            );
            const type: string = clc.red("DELETE".padEnd(10, " "));
            this.logger.info(`${type} ${Params.PROTOCOL}${Params.URL}:${Params.PORT}${routeName}/:id`);
        }
    }

}

import pino, {Logger} from "pino";
import * as path from "path";
import {Params} from "../core/Params";

export class Log {

    private logger: Logger;
    private filename: string;

    constructor(filename: string = __filename) {
        this.filename = filename;
        this.logger = pino({
            name: Params.NAME,
            level: Params.LEVEL_LOG,
            transport: {
                targets: [
                    {
                        target: "pino-pretty",
                        options: {
                            singleLine: true,
                            translateTime: "dd/mm HH:MM:ss",
                            ignore: "pid,hostname,filename",
                            messageFormat: "{filename} - {msg}",
                        }
                    },
                    {
                        target: "pino/file",
                        options: {
                            destination: `${Params.LOG_DIR}/app.log`
                        },
                    },
                ],
            }
        });
    }

    public debug(message: any, filename: string = this.filename) {
        const ch = this.logger.child({filename: path.basename(filename).padEnd(15, " ")});
        ch.debug(message);
    }

    public info(message: any, filename: string = this.filename) {
        const ch = this.logger.child({filename: path.basename(filename).padEnd(15, " ")});
        ch.info(message);
    }

    public warn(message: any, filename: string = this.filename) {
        const ch = this.logger.child({filename: path.basename(filename).padEnd(15, " ")});
        ch.warn(message);
    }

    public success(message: any, filename: string = this.filename) {
        const ch = this.logger.child({filename: path.basename(filename).padEnd(15, " ")});
        ch.info(message);
    }

    public error(message: any, filename: string = this.filename) {
        const ch = this.logger.child({filename: path.basename(filename).padEnd(15, " ")});
        ch.error(message);
    }
}

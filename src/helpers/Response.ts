import {IResponse} from "../interface/IRouteController";
import {Params} from "../core/Params";
import {Log} from "../helpers/Log";

export class Response {
    private static logger: Log = new Log(__filename);

    static success(res: IResponse, data: any = {}, message: string = "OK") {

        if (Params.DEBUG_POST) {
            this.logger.debug({
                url: res.req.url,
                data: data
            });
        }

        this.json(res, 200, message, data);
    }

    static error(res: IResponse, message: string = "", messageLog: any = "") {

        if (messageLog) {
            this.logger.error(messageLog);
        }

        this.json(res, 500, message);
    }

    static json(res: IResponse, code: number, message: string = "OK", data: any = {}) {
        if (!res.writableEnded) {
            res.status(code);
            res.set("Connection", "close");
            res.send({
                code: code,
                data: data,
                message: message
            });
        }
    }

    static text(res: IResponse, code: number = 200, data: string = "") {
        if (!res.writableEnded) {
            res.status(code);
            res.set("Connection", "close");
            res.type("text").send(data);
        }
    }

    static file(res: IResponse, file: string, filename: string) {
        if (!res.writableEnded) {
            res.set("Connection", "close");
            res.download(file, filename);
            res.end();
        }
    }

}

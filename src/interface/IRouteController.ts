import Express from "express";
import * as core from "express-serve-static-core";

export interface IRouteController {
    get?(req: IRequest, res: IResponse): void;

    post?(req: IRequest, res: IResponse): void;

    put?(req: IRequest, res: IResponse): void;

    delete?(req: IRequest, res: IResponse): void;

    getItem?(req: IRequest, res: IResponse): void;

    middleware?(req: IRequest, res: IResponse, next: INextFunction): void;
}

export interface IRequest<
    P = core.ParamsDictionary,
    ResBody = any,
    ReqBody = any,
    ReqQuery = core.Query,
    Locals extends Record<string, any> = Record<string, any>,
> extends Express.Request<P, ResBody, ReqBody, ReqQuery, Locals> {
    context?: any;
}

export interface IResponse<
    ResBody = any,
    Locals extends Record<string, any> = Record<string, any>,
> extends Express.Response<ResBody, Locals> {
    context?: any;
}

export interface INextFunction extends Express.NextFunction {
    context?: any;
}